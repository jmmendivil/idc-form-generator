export default class Textarea extends React.Component {
  constructor () {
    super()
    this.state = {
      text: '',
    }
  }


  render () {
    const { text } = this.state
    const { onChange, defaultValue } = this.props
    const columnClass = !text ? 'large-6 animated fadeIn' : 'large-5'
    const columnStyle = { 'transition': 'all 0.4s ease-out' }

    return (
      <div class={columnClass + ' columns'} style={columnStyle}>
        <label>
          <h4>Document input</h4>
          <textarea rows='18' onChange={onChange} defaultValue={defaultValue}/>
        </label>
        <p class='help-text'>Hint: Use [[ input-name ]] to make an input element inside your form.</p>
      </div>
    )
  }
}
