export default class Document extends React.Component {
  constructor () {
    super()
  }

  render () {
    const { className } = this.props
    const { content, close } = this.props
    return (
      <div class={ className }>
        <div class='row modal-content animated fadeInDown'>
          <button class="close-button" onClick={close}>
            <span aria-hidden="true">×</span>
          </button>
          <div class='column'>
            <h4>Generated Document</h4>
            <hr />
            <div dangerouslySetInnerHTML={{__html: content}} />
            <hr />
            <button class='button'>Download PDF</button>
          </div>
        </div>
      </div>
    )
  }
}
