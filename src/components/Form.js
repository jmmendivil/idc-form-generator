export default class Form extends React.Component {
  render () {
    const { content } = this.props

    const columnClass = content ? 'animated fadeIn' : 'hide invisible'
    const columnStyle = { 'animationDelay': '0.4s' }

    return (
      <div class={columnClass + ' columns'} style={columnStyle} >
        <h4>Generated Form</h4>
        <form dangerouslySetInnerHTML={{__html: content}} />
        <hr />
        {this.props.children}
      </div>
    )
  }
}
