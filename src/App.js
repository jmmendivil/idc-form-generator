// TODO:
// - Clean match1 and test if already exists
// - generate dynamic input width based in content
// - format paragraphs
// - markdown?
// - auto fill same name inputs with previous content

import Textarea from './components/Textarea'
import Form from './components/Form'
import Document from './components/Document'
import showdown from 'showdown'

const Markdown = new showdown.Converter()
Markdown.setOption('simplifiedAutoLink', true);
Markdown.setOption('excludeTrailingPunctuationFromURLs', true);
Markdown.setOption('strikethrough', true);
Markdown.setOption('simpleLineBreaks', true);
Markdown.setOption('tables', 'true');


export default class App extends React.Component {
  constructor () {
    super()
    this.state = {
      input: '',
      generated: '',
      final: '',
      modal: false
    }
  }

  // Default data (test)
  componentWillMount () {
    const defaultValue = `### Title
Quod *expedita* quam [[possimus]] _quasi_ quo non rerum soluta. Qui dolorem quaerat ad. Cum [[voluptatibus]] perferendis impedit aliquam nostrum non qui. Omnis amet repellat voluptas rem accusamus. Impedit officia quaerat sed aut cum provident facilis. Magni accusantium eaque qui eos.

---

Facere ad beatae et ea libero. Unde assumenda et doloribus quod iusto. Velit ut ea asperiores repudiandae nihil sit. Iste repellat nihil corporis laborum quia odit recusandae. Deleniti voluptatem dolorem dolores repudiandae qui quo. Officia totam et ut nemo eum veniam quia dolores.
### Table
| h1    |    h2   |      h3 |
|:------|:-------:|--------:|
| 100   | [a][1]  | [[input]] |
| *foo* | **bar** | ~~baz~~ |`

    let generated = this.textToInput(defaultValue)
    generated = Markdown.makeHtml(generated)
    this.setState({ generated, defaultValue, input: defaultValue })
  }

  handleInput (evt) {
    const input = evt.target.value
    let generated = this.textToInput(input)
    generated = Markdown.makeHtml(generated)
    this.setState({ generated, input })
  }

  getInputs (string, cb) {
    let match = []
    const regex = /\[\[\s*([^\[\]\s]+(?:[ \t]+[^\[\]\s]+)*)\s*\]\]/ig
    while ((match = regex.exec(string)) !== null) {
      cb(match)
    }
  }

  textToInput (text) {
    const width = 35
    this.getInputs(text, match => {
      const matchName = match[1].replace(/\s/g, '-')
      text = text.replace(
        match[0],
        `<input type="text" name="${matchName}" placeholder="${match[1]}" style="width: ${width}%; display: inline;" />`
      )
    })
    return text
    // return text.replace(/(?:\r\n|\r|\n)/g, '<br />');
  }

  formToText () {
    let text = this.state.input
    this.getInputs(text, match => {
      const matchName = match[1].replace(/\s/g, '-')
      const inputValue = document.querySelector(`input[name=${matchName}]`).value || match[1]
      text = text.replace(
        match[0],
        `<strong>${inputValue}</strong>`
      )
    })

    const final = Markdown.makeHtml(text)
    this.setState({ final })
  }

  closeModal () {
    // Fix animation
    this.setState({ final: null })
  }

  render () {
    const { generated, defaultValue, final, modalOut } = this.state
    let documentClass = final ? 'animated fadeIn modal' : 'hide'

    return (
      <div>
        <div class='row align-center'>
          <Textarea onChange={::this.handleInput} defaultValue={defaultValue}/>
          <Form content={generated}>
            <p><button class='button expanded' onClick={::this.formToText}>Generate document</button></p>
            {generated.indexOf('<input') > -1 ? <p class='help-text'>* No filled Input elements will get name value as default.</p> : '' }
          </Form>
        </div>
        { final ? <hr /> : '' }
        <Document class={documentClass} content={final} close={::this.closeModal}/>
      </div>
    )
  }
}
        // <Document class={documentClass + ' row align-center'} content={final}/>
