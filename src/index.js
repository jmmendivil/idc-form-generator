import { AppContainer } from 'react-hot-loader'
import App from './App.js'

ReactDOM.render(
  <AppContainer>
    <App />
  </AppContainer>,
  document.getElementById('root')
)
